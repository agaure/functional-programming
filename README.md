# Functional programming

This is the course repository for my [Functional programming] course.


* [Course Homepage][functional programming]

* [Course Wiki][wiki]

* [Issue tracker][issues]

For all queries and clarification please raise issues on the issue
tracker. Also use the issue tracker as a discussion board for the
course.



[functional programming]: <http://cse.iitk.ac.in/users/ppk/teaching/Functional-Programming/index.html>
[wiki]: <https://bitbucket.org/ppk-teach/functional-programming/wiki>
[issues]: <https://bitbucket.org/ppk-teach/functional-programming/issues>
